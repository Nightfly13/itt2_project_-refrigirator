---
title: '19S ITT2 Project'
subtitle: 'Project plan'
authors: ['Anthony James Peak anth0662@edu.eal.dk', 'Kasper Jensen kasp7547@edu.eal.dk', 'Laurynas Medvedevas laur176n@edu.eal.dk']
main_author: 'Kasper Jensen'
date: \today
email: 'kasp7547@edu.eal.dk'
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose

The purpose of this project is to make an IoT system using light sensors that can control the lighting of a room to be at a constant level.

# Goals

The overall system that is going to be build looks as follows

![project_overview](project_overview.png)

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the serial connection to the raspberry pi.
* Raspberry Pi: Minimal Linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Documentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to the Easter holidays.

See the lecture plan for details.


# Organization

    • Steering Committee: TBD
    • Project Manager: Kasper Jensen
    • Other Members: Laurynas Medvedevas, Anthony James Peak
    • External Resources: TBD

# Contact Information
The contact information for each member of the group is provided below.

    • Kasper Jensen:
      - E-mail: 'kasp7547@edu.eal.dk'
      - Discord: 'Kasper#4821'
    • Anthony James Peak:
      - E-mail: 'anth0662@edu.eal.dk'
      - Discord: 'ExplosionFace#5594'
    • Laurynas Medvedevas:
      - E-mail: 'laur176n@edu.eal.dk'
      - Discord: 'Nightfly#2706'

# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contribute.

# Risk assessment

Fried hardware:
    Be careful when handling the electronics and make sure to discharge yourself frequently.
Unexpected illnesses and/or injuries:
    Try to avoid injuries, and if someone is injured have another team member fill in for their work.
Disastrous management:
    Maintain constant communication with team members and have well laid out tasks so that nobody is left doing nothing.

# Stakeholders

    • The project group
    • The teachers
    • Individuals from the Botanical industry

# Communication

We will have weekly status meetings with the stakeholders to cover the project's progress so far, as well as the next steps and future plans.
The channels that are used for communication include GitLab for assigning tasks and Discord for direct communication.

# Perspectives

This project will serve as a template for other similar project, like the second one in ITT2 or in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

Does the project work as intended?

# References

None at this time.
