This folder contains everything needed to reproduce the temperature system project made by group refrigerator.

The system makes the user able to read a live temperature on a webpage running on a local network.

-----------------------------------------------------------------------------------------------------------------------------

Hardware/Software that was used in order to create the system:


Software:

* Atmel Studio 7.0
* Raspbian Strech Lite ISO 

    
Hardware:

* Analog temperatur sensor (TMP36)
* LED (Green)
* 133 ohms resistor
* Four-legged press button
* ATmega328P
* Raspberry Pi 3 B+
* Level shifter (5V to 3.3V)
* Small soldering board
* SRX-240 Juniper router
* GPIO jumper cables (female/female)
* Ethernet Network (CAT 5+) cables, with RJ45 connections
* Pin headers


Tools:

* Computer with USB and Windows OS
* Miro-USB to USB cable
* Soldering iron
* Tin
* Wire cutter

-----------------------------------------------------------------------------------------------------------------------------


#### Phase 1 - Building the physical board

* [Produce the sensor board - Solder the component to the solder board.](https://gitlab.com/laur176n/itt2_project_-refrigirator/blob/master/Minimal%20System/Documentation/Building%20the%20board.md)


#### Phase 2 - Installing software

* [Write or flash the C code to the ATmega using Atmel Studio.](https://gitlab.com/laur176n/itt2_project_-refrigirator/blob/master/Minimal%20System/Documentation/flashing%20to%20the%20atmega.md) 
* [Install the OS and transfer the code onto the first Raspberry Pi.](https://gitlab.com/laur176n/itt2_project_-refrigirator/blob/master/Minimal%20System/Documentation/rpi%20install%201.md)
* [Install the OS, transfer the code and install the webserver on the seconds Raspberry Pi.](https://gitlab.com/laur176n/itt2_project_-refrigirator/raw/master/Minimal%20System/images/plswait.png)

#### Phase 3 - Connecting everything together

* [Connect Raspberry Pi 1 and the ATmega328P board with the use of UART.](https://gitlab.com/laur176n/itt2_project_-refrigirator/blob/master/Minimal%20System/Documentation/rpi%20install%201.md)
* [Connect to the SRX router, and set up the configuration followed by a commit.](https://gitlab.com/laur176n/itt2_project_-refrigirator/blob/master/Minimal%20System/Documentation/Configuring%20The%20Router.md)
* [Connect Raspberry Pi 1 and 2 together through the router Ethernet.](https://gitlab.com/laur176n/itt2_project_-refrigirator/blob/master/Minimal%20System/Documentation/Configuring%20The%20Router.md)
* [Connect the sensor board to the Raspberry Pi 1.](/Minimal%20System/images/Hcsmr6x_1_.png) (Look below, there is an image of the entire system connected)


#### Phase 4 - Testing the system


* Test the system; If it is not working, look back at the steps and check if you did everything correctly


-----------------------------------------------------------------------------------------------------------------------------

If you are still running into issues, you can contact any of the members via email:


* Laurynas Medvedevas (laur176n@edu.eal.dk) 
* Anthony James Peak (anth0662@edu.eal.dk) 
* Kasper Jensen (kasp7547@edu.eal.dk)


-----------------------------------------------------------------------------------------------------------------------------

**Overview**

![IMG](/Minimal%20System/images/Hcsmr6x_1_.png)
