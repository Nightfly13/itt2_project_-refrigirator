from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app, resources=r'/*') # to allow external resources to fetch data
api = Api(app)

from atmega import *

serial_cfg = {  "dev": "/dev/ttyS0", "baud": 9600 }
atmega = Atmega()
atmega.connect(serial_cfg["dev"], serial_cfg["baud"])

gpios = {"A0": { 'value': 0},
         "A1": { 'value': 0},
         "B0": { 'value': 0},}

def readtemp(gpio_name):
    read = atmega.readtempcelcius()
    gpios[gpio_name]["value"] = read[1] #read[0]
    gpios[gpio_name]["temperature"] = read[1]

def blink(count):
    atmega.send("blink_interval2000")
    atmega.send("blink"+ str(count))
    print(count)


@api.resource("/")
class url_index( Resource ):
    def get( self ):
       returnMessage = {"message": "Yes, it works" }
       return returnMessage

@api.resource("/gpio")
class url_gpio( Resource ):
    def get( self ):
        returnMessage = gpios.keys()
        return returnMessage

@api.resource("/gpio/<gpio_name>")
class url_gpio_name( Resource ):
    def get( self, gpio_name ):
        try:
            if ("A1" == gpio_name ):
                readtemp("A1")
            if ("A0" == gpio_name):
                blink(int(gpios["A0"]["value"])*3)
            return gpios[ gpio_name ]
        except KeyError, ex:
            return { "message": "key error '%s' is not a GPIO name"%(gpio_name,) }, 400

    def put( self, gpio_name ):
        try:
            gpios[ gpio_name ]['value'] = request.json['value']
            return "", 204
        except KeyError, ex:
            return { "message": "key error: Either '%s' is not a GPIO name or data is undefined (received: '%s')"%(gpio_name, str(request.form)) }, 400

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
