this stuff makes the temperature accessible via a restful api in JSON format

rpi notes:
pip install flask
pip install flask-restful
pip install flask-cors

run: 
sudo -E python rest.py
-E is necessary because by default sudo clears environment variables including PYTHONPATH. this was the easy fix. the better fix is here

https://stackoverflow.com/questions/25346171/cant-import-module-when-using-root-user

atmega notes:
atmega is sensitive to interference, i had to hang it from the desk by wires to stop it generating constant errors over serial.
its running pretty much exactly the code we were given.
it is set up to read pin A1.
