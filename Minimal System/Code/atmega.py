#!/usr/bin/env python3

import serial
import time

class Atmega:
    ser = None
    def connect(self, port, baud):
        self.ser = serial.Serial(port, baud, timeout=1)

    def send(self, message):
        while(True):
            try:
                self.ser.write( message+"\n".encode() )
                return
            except SerialException:
                print("serial exception")

    def readtempraw(self):
        while(True):
            try:
                self.ser.write( "getadcval\n".encode() )
                reply = self.ser.readline().decode()
                if(reply[0] == ">"):
                    reply = reply[2:-2]
                    if (reply != ""):
                        return (int(reply))
            except ValueError, SerialException:
                print (reply)

    def readtempcelcius(self):
        read = self.readtempraw()
        return (read, (read*430)/1024.0 - 50 )
