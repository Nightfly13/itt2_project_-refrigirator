## Configure The Router

**Router Config-script installation**
* Access the router console using PuTTY, and enter "cli" in order to access the command line interface.
* Type in "edit" to access configuration mode.
* Type in "load override terminal" to access the override terminal.
* Copy and paste the contents of the file named "config.json" into the override terminal.
* When finished, press CTRL+D (for "Done")
* Type in "commit" and wait for the new configuration to work.

## Connecting Cables to the Router

* Connect an Ethernet cable (Cat5e or higher recommended) to the router interface ge-0/0/9 and connect it to the school network via the unmanaged switch.
* Connect an Ethernet cable (Cat5e or higher recommended) from router interface ge-0/0/9 to RPi2.

## Set static IP on RPi2
Interface ge-0/0/9 is on subnet 192.168.20.0/24. The static IP on RPi2 should be 192.168.30.10/24, with Default Gateway set to 192.168.20.1.

## Connecting RPi1 and RPi2 to the Router

* RPi1 will be connected to the network via WiFi, and will access the webserver at IP 10.217.19.211.
* RPi2 will be connected to the router via an Ethernet cable (see **Connecting Cables to the Router**)