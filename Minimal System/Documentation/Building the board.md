Phase 1 - Building the physical hardware

## Building the Sensor Board

Components:
- 1x TMP36 Analog Temperature Sensor
- 1x LED (Green)
-	1x 133 Ohm resistor
-	1x Momentary NO push-button (Non-latching, Normally Open)
-	Veroboard or stripboard
-	5-pin Pin Header

Tools:
- Soldering iron
- Solder
- Wire cutter
- Wire stripper (optional)

## Building the Board

1. Create a layout for the sensor board in Fritzing, or use the one [found in this project](https://gitlab.com/laur176n/itt2_project_-refrigirator/blob/master/Minimal%20System/images/Sensor_board.png).
2. Using the wire cutter, cut a piece of veroboard/stripboard that has the necessary size for the board, without being oversized.
3. Mount the components on the board, according to the layout from point 1. To make it as easy as possible to get the components to stay in place, mount the physically smallest ones first. The first component that should be mounted is the resistor, followed by the push button, then the LED and temperature sensor, and finally the pin header. Be careful to apply only enough heat to melt the solder. Excessive heat will cause the copper to let go of the board itself, which can lead to a hardware malfunction.

## Connecting to the ATMega328P

4. Once all components are mounted and soldered, connect the sensor board to the ATMega with jumper wires.