## Install the OS

* Install Raspbian Stretch Lite on the Raspberry ( Guide can be found in the following link -> [link](https://medium.com/@danidudas/install-raspbian-jessie-lite-and-setup-wi-fi-without-access-to-command-line-or-using-the-network-97f065af722e) )
* Access the Raspberry

## Transfer the python code.

* Download all files and folders in [placeholder](https://gitlab.com/laur176n/itt2_project_-refrigirator/raw/master/Minimal%20System/images/plswait.png) and transfer to the Raspberry Pi 2.

## Install the webserver.

* Install [requirements](https://gitlab.com/laur176n/itt2_project_-refrigirator/raw/master/Minimal%20System/images/plswait.png) for the RESTfull API:


        pip3 install -r requirements.txt


* Run the webserver service:


        python3 server.py


## Access website with curl to test it works

* In the bash terminal enter following command -

        curl 127.0.0.1:5000