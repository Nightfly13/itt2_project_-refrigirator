## Overview diagram

![Overview Diagram](https://i.imgur.com/04cqMWp.png)

## Minimal system documents

A guide to recreating the minimal system can be found [here](https://gitlab.com/laur176n/itt2_project_-refrigirator/tree/master/Minimal%20System).
